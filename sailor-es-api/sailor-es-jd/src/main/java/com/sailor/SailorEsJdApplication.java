package com.sailor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SailorEsJdApplication {

	public static void main(String[] args) {
		SpringApplication.run(SailorEsJdApplication.class, args);
	}

}
